0. Search before posting (to avoid duplicated content).
1. Treat everyone with respect. Absolutely no harassment, put downs, bullying, witch hunting, sexism, racism, anti-LGBTQ+ speech, hate speech or degrading content will be tolerated.
2. No spam or self-promotion (server invites, advertisements, etc) without permission from a staff member. This includes DMing fellow members.
3. No adult (18+), explicit, or controversial messages.
4. No political or religious topics.
5. No flooding the chat with messages. Do not type in ALL CAPS.
6. No NSFW or obscene content. This includes text, images, or links featuring nudity, sex, hard violence, or other graphically disturbing content.
7. No excessive cursing.
8. No advertising  (Only with Permission).
9.  No referral links.
10. No begging or repeatedly asking for help in the chat. Repeatingly asking basic questions will lead to administrative action.
11. No offensive names.
12. Sending/Linking any harmful material such as viruses, IP grabbers or harmware results in an immediate and permanent ban.
13. Use proper grammar and spelling and don't spam.
14. Post content with the correct flairs (if applicable).
15. Don't post someone's personal information without permission.
16. Listen to what Mods says. Do not argue with staff. Decisions are final.
17. Act civil.
18. Moderators and Admins reserve the right to delete and edit posts.
19. Do not perform or promote the intentional use of glitches, hacks, bugs, and other exploits that will cause an incident within the community and other players.
20. If you see something against the rules or something that makes you feel unsafe, let the mods know. We want this community to be a welcoming space!
21. We are not resposible for bugs or failures from Discord or other 3rd party providers like bots.
22. By using this sub you agree to these rules and to Reddit's global rules. Infringement of any of these rules will lead to immediate ban and legal action if needed.